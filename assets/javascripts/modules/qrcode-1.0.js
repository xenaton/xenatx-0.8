// *****************
// GENERATE QR CODE
// *****************

(function() {

    "use strict";

    // qrcode character counter
    var displayEnteredCharQRC = document.getElementById("displayEnteredCharQRC");  
    var variousContent = document.getElementById("variousContent");   
    const qrcodeCharCounter = () => {
        let numOfEnteredChars = variousContent.value.length;
        displayEnteredCharQRC.textContent = numOfEnteredChars;
    };
    variousContent.addEventListener("input", qrcodeCharCounter);

    // multiples QR Codes counter
    function addNewQrCode(index, multiple) {
        var qrcodeWrapper = document.getElementById('qrcodeWrapper');
        if(multiple) {
            var newDivTitle = document.createElement('p');
            newDivTitle.setAttribute("class", "h4");
            newDivTitle.innerText = 'QR Code n°' + (index+1);
            qrcodeWrapper.appendChild(newDivTitle);
        }
        var newDivQRCode = document.createElement('div');
        newDivQRCode.setAttribute("id", "qrcode" + index);
        newDivQRCode.setAttribute("class", "qrcode__pad");
        qrcodeWrapper.appendChild(newDivQRCode);

    }
   
    // factory to generate QR Code
    function makeCode () {		
        // set transport capacity of QR Code
        var qrcodeCapacity = document.getElementById('qrcodeCapacity');
        if(qrcodeCapacity){
            var variousContentBlock = qrcodeCapacity.value;
        }

        var variousContent = document.getElementById('variousContent').value;
        var variousContentMaxCharacters = variousContentBlock * 100;

        if(variousContent == '') {
            document.getElementById("errorQRCode").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Veuillez indiquer un contenu à transformer en QR Code.</p>'
            return;
        }

        if(variousContent.length > variousContentMaxCharacters) {
            document.getElementById("errorQRCode").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> La taille des informations est trop importante pour un transfert par QR Code. Elle excède '+ variousContentMaxCharacters +' caractères. Utilisez le Transmetteur Acoustique en Morse.</p>'
            return;
        }
        
        if(variousContent.length > variousContentBlock) {
            // var blocks = variousContent.match(/.{1,700}/g);
            // equivalent of match above but with ability to use variables (too hard to place vars in match function)
            function splitToSubstrings(str, nChar) {
                const arr = [];
                for (var i = 0; i < str.length; i += nChar) {
                    arr.push(str.slice(i, i + nChar));
                }
                return arr;
            }

            var blocks = splitToSubstrings(variousContent, Number(variousContentBlock));  

            blocks.forEach((item, index) => {
                addNewQrCode(index, true);
                var qrcode = new QRCode("qrcode" + index, {
                    correctLevel : QRCode.CorrectLevel.L
                });
                qrcode.makeCode(item);
            });
        } 
        else {
            addNewQrCode(0, false);
            var qrcode = new QRCode("qrcode", {
                correctLevel : QRCode.CorrectLevel.L
            });
            qrcode.makeCode(variousContent);
        }
    }

    // generate one or many QR Codes
    var generateQRCode = document.getElementById('generateQRCode');
    if(generateQRCode){
        generateQRCode.addEventListener('click', function(e){
            e.preventDefault();
            var errorQRCode = document.getElementById("errorQRCode");
            errorQRCode.innerHTML = '';

            const myNode = document.getElementById("qrcodeWrapper");
                  myNode.innerHTML = '';
            qrcodeWrapper.style.display = 'block';
            setDisabled('generateQRCode');
            makeCode();
        })
    }

    // cancel
    var cancelQRCode = document.getElementById('cancelQRCode');
    if(cancelQRCode){
        cancelQRCode.addEventListener('click', function(e){
            e.preventDefault();
            var errorQRCode = document.getElementById("errorQRCode");
            errorQRCode.innerHTML = '';

            const variousContent = document.getElementById("variousContent");
                  variousContent.value = '';

            const nodeQrcode = document.getElementById("qrcode");
                  nodeQrcode.innerHTML = '';

            const nodeQrcodeWrapper = document.getElementById("qrcodeWrapper");
                  nodeQrcodeWrapper.innerHTML = '';

            qrcodeWrapper.style.display = 'block';

            var displayEnteredCharQRC = document.getElementById("displayEnteredCharQRC");  
            displayEnteredCharQRC.textContent = 0;

            removeDisabled('generateQRCode');
        })
    }
})();