// *********
// HASH FILE
// *********

function sha512sum(checksumTrigger, checksumResult) {
    var oFile = document.getElementById(checksumTrigger).files[0];
    var sha512 = CryptoJS.algo.SHA512.create();
    var read = 0;
    var unit = 1024 * 1024;
    var blob;
    var reader = new FileReader();
    reader.readAsArrayBuffer(oFile.slice(read, read + unit));
    reader.onload = function(e) {
        var bytes = CryptoJS.lib.WordArray.create(e.target.result);
        sha512.update(bytes);
        read += unit;
        if (read < oFile.size) {
            blob = oFile.slice(read, read + unit);
            reader.readAsArrayBuffer(blob);
        } else {
            var hash = sha512.finalize();
            document.getElementById(checksumResult).innerHTML = '<p class="alert alert-info"><i class="bi bi-hash"></i> <strong>Empreinte SHA-512</strong> :<br> ' + hash.toString(CryptoJS.enc.Hex).toUpperCase() +'</p>'
        }
    }
}

(function() {

    "use strict";

    document.getElementById('checksumZip').addEventListener('change', function(e) {
        e.preventDefault();
        
        var checkSumResult = document.getElementById('checkSumZipResult');
        checkSumResult.innerHTML = '';

        sha512sum('checksumZip','checkSumZipResult');
    })

    document.getElementById('checksumFile').addEventListener('change', function(e) {
        e.preventDefault();
        
        var checkSumResult = document.getElementById('checkSumFileResult');
        checkSumResult.innerHTML = '';

        sha512sum('checksumFile','checkSumFileResult');
    })
              
})();
