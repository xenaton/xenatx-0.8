// **********
// SIGNATURE
// **********

/*----------------------
Show Password signature
-----------------------*/

(function($) {

    "use strict";

    $("#show_password_sign").click(function() {

        if ($('#show_password_sign').hasClass('bi-eye-fill')) {
            $('#sign_form_password').attr('type', 'text');
            $('#show_password_sign').removeClass('bi-eye-fill').addClass('bi-eye-slash-fill');
        }
        else {
            $('#sign_form_password').attr('type', 'password');
            $('#show_password_sign').removeClass('bi-eye-slash-fill').addClass('bi-eye-fill');
        }
    });

})(window.jQuery);


(function() {

    "use strict";

    document.getElementById('privKeyFile')
        .addEventListener('change', function() {
        
        var fr=new FileReader();
        fr.onload=function(){
            addText(fr.result, "inputPrivateKey");
        }
        fr.readAsText(this.files[0]);
    })

    document.getElementById('pubKeyFile')
        .addEventListener('change', function() {
        
        var fr=new FileReader();
        fr.onload=function(){
            addText(fr.result, "inputPublicKey");
        }
        fr.readAsText(this.files[0]);
    })

    // SIGN A MESSAGE
    // **************
    var sign = document.getElementById('sign');
    if(sign){
        sign.addEventListener('click', function(e){
            e.preventDefault();
            var inputPrivateKey = document.querySelector('#inputPrivateKey').value;
            
            var errorSign = document.getElementById("errorSign");
            errorSign.innerHTML = '';
            
            var sign_form_password = document.querySelector('#sign_form_password').value;
            if('' == sign_form_password) {                           
                document.getElementById("errorSign").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Le mot de passe de votre clé privée doit être indiqué.</p>'
                return;
            }

            var messageToSign = document.querySelector('#messageToSign').value;
            if('' == messageToSign) {
                document.getElementById("errorSign").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Merci d\'indiquer un contenu textuel à signer.</p>'
                return;
            }
            
            // a private key is at least > 100 (we could add some control like this string must begin with -----BEGIN PGP PUBLIC KEY BLOCK----- and end with -----END PGP PUBLIC KEY BLOCK----- )
            if(inputPrivateKey.length < 100) {
                document.getElementById("errorSign").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Clé privée absente ou incorrecte.</p>'
                return;
            }

            (async () => {
                            
                const privateKeyArmored = inputPrivateKey; // encrypted private key

                // what the private key is encrypted with
                const passphrase = sign_form_password;

                try {
                    const privateKey = await openpgp.decryptKey({
                        privateKey: await openpgp.readPrivateKey({ armoredKey: privateKeyArmored }),
                        passphrase
                    });
                    
                    const message = await openpgp.createMessage({ text: messageToSign }); // input as Message object
                    const detachedSignature = await openpgp.sign({
                        message, // Message object
                        signingKeys: privateKey,
                        detached: true
                    });
                    addText(detachedSignature, "signature");
                    removeDisabled('signature');

                } 
                catch (e) {
                    document.getElementById("errorSign").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Signature : ' + e.message + '.</p>'
                    return;
                }
                
            })();

        });
    } // end if sign

    // CHECK THE SIGNATURE OF A MESSAGE
    // *********************************
    var checkSign = document.getElementById('checkSign');
    if(checkSign){
        checkSign.addEventListener('click', function(e){
            e.preventDefault();

            var errorVerifySign = document.getElementById("errorVerifySign");
            errorVerifySign.innerHTML = '';

            var inputPublicKey = document.querySelector('#inputPublicKey').value;
            var detachedSignature = document.querySelector('#detachedSignature').value;
            var messageToCheckSign = document.querySelector('#messageToCheckSign').value;
            
            if('' == inputPublicKey || '' == detachedSignature || '' == messageToCheckSign) {
                document.getElementById("errorVerifySign").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Des informations importantes sont manquantes ou incorrectes, merci de vérifier.</p>'
                return;
            }

            (async () => {                                  
                // put keys in backtick (``) to avoid errors from spaces or tabs
                const publicKeyArmored = inputPublicKey;
                
                try {
                    const publicKey = await openpgp.readKey({ armoredKey: publicKeyArmored });
                } 
                catch (e) {
                    errorVerifySign.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Signature : ' + e.message + '.</p>'
                    return;
                }

                const publicKey = await openpgp.readKey({ armoredKey: publicKeyArmored });
                const message = await openpgp.createMessage({ text: messageToCheckSign });

                try {
                    const signature = await openpgp.readSignature({
                        armoredSignature: detachedSignature // parse detached signature
                    });

                    const verificationResult = await openpgp.verify({
                        message, // Message object
                        signature,
                        verificationKeys: publicKey
                    });

                    const { verified, keyID } = verificationResult.signatures[0];
                    try {
                        await verified;
                        errorVerifySign.innerHTML = `<p class="alert alert-success"><i class="bi bi-bookmark-check"></i> Signature vérifiée et valide. Ce contenu textuel a bien été signé par la clé privée soeur "jumelle" de cette clé publique dont l'identifiant est [ ` + keyID.toHex().toUpperCase() + ` ].</p>`;
                    } 
                    catch (e) {
                        errorVerifySign.innerHTML = `<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ATTENTION - La signature de ce contenu textuel n'est pas valide. La clé publique indiquée n'est peut-être pas la soeur "jumelle" de la clé privée ayant servi à la signature. Autre raison : si dans les détails suivants il est indiqué [ Signed digest did not match ] cela signifie que le contenu textuel ne correspond pas à l'original. Il peut avoir été falsifié ou vous avez mal recopié l'original. Détails: ` + e.message + `.</p>`;
                    }
                } 
                catch (e) {
                    document.getElementById("errorVerifySign").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Vérification de signature : ' + e.message + '.</p>';
                    return;
                }
                
            })();

        });
    } // end if sign                  
})();
