// *****
// i18n
// *****

i18next.init({
    lng: 'fr',
    fallbackLng: 'fr',
    interpolation: {
        escapeValue: false,
    },
    debug: true,
    resources
}, function(err, t) {
    // init set content
    updateContent(lgKeys);
});

function updateContent(lgKeys) {
    for (var i = 0, length = lgKeys.length; i < length; i++) {
        var elExploded = lgKeys[i].split('.');
        var elConcatenated = elExploded.join('-');
        var temp = lgKeys[i];
        document.getElementById(elConcatenated).innerHTML = i18next.t(temp);
    }
}

function changeLng(lng) {
    i18next.changeLanguage(lng);
}

i18next.on('languageChanged', () => {
    updateContent(lgKeys);
});